const { Client, Intents, Permissions } = require('discord.js');
const client = new Client({
  intents: [
    Intents.FLAGS.GUILDS,
    Intents.FLAGS.GUILD_MESSAGES,
    Intents.FLAGS.GUILD_MESSAGE_REACTIONS
  ]
})

const botToken = "OTE2MzM5MzgxMzAyODgyNDA0.YaotjQ.NTYRXiZcZWuZ7nf-PibmWm6tNFA"
const translate = require('@vitalets/google-translate-api');

/* add bot to server link
  https://discord.com/oauth2/authorize?client_id=916339381302882404&scope=bot&permissions=0
*/

const channels = []
let outputChannel = null
let proxy = false
let silent = false

const handleCommand = (msg) => {
  if (msg.content.startsWith('/zakibot silent')) {
    silent = true
  }
  if (msg.content.startsWith('/zakibot verbose')) {
    silent = false
  }

  if (msg.content.startsWith('/zakibot proxy')) {
    proxy = true
    if (!silent) msg.reply("Zakibot switched to proxy mode, say /zakibot here to set the output channel." + ` ${outputChannel? outputChannel.name : "No output channel set"}`)
    if (channels.includes(msg.channelId)) return
    channels.push(msg.channelId)
    if (!silent) msg.reply("Zakibot is now translating this channel, type /zakibot stop, to stop translating this channel")
    return
  }

  if (msg.content.startsWith('/zakibot stop proxy')) {
    proxy = false
    if (!silent) msg.reply("Zakibot switched to direct mode.")
    return
  }

  if (msg.content.startsWith('/zakibot here')) {
    outputChannel = msg.channel
    if (!silent) msg.reply("Zakibot output channel for proxy mode is set to this channel")
    return
  }

  if (msg.content.startsWith('/zakibot start')) {
    // add channel to the list to translate
    if (channels.includes(msg.channelId)) return
    channels.push(msg.channelId)
    if (!silent) msg.reply("Zakibot is now translating this channel, type /zakibot stop, to stop translating this channel")
    return
  }

  if (msg.content.startsWith('/zakibot stop')) {
    channels.splice(channels.findIndex( c => c == msg.channelId), 1)
    if (!silent) msg.reply("Zakibot is stopped translating this channel")
    return
  }

  if (msg.content.startsWith('/zakibot help')) {
    msg.reply("Zakibots available  reactions: 🇵🇱, 🇺🇸, 🇭🇺, 🇩🇪, 🇮🇹, 🇨🇿. To start translating a channel type /zakibot start, to stop type /zakibot stop. For proxy mode say /zakibot proxy and set the channel by typing /zakibot here in one of the channels. Enable silent mode with /zakibot silent or turn it off by /zakibot verbose.")
    return
  }
}


client.on('messageCreate', msg => {

  let { id, content, author } = msg
  if (content.startsWith('/zakibot')) return handleCommand(msg)

  if (msg.author.bot || !channels.includes(msg.channelId)) return

  if (!silent) msg.react('⌛')


  translate(String(content), {to: 'en'}).then(res => {
    // in order for this to work you need special permissions by an added role
    if (!silent) msg.reactions.removeAll()

    if (res.from.language.iso == 'en' || !res.from.language.iso) return
    if (proxy) {
      outputChannel.send("`" + author.username + "`" + ' in ' + "`" + msg.channel.name + "`" + ': ' + res.text)
    } else {
      msg.reply( "`" + author.username + "`" + ' means: ' + res.text)
    }
  }).catch(err => console.error(err))
})

client.on('messageReactionAdd', (reaction, user) => {
  let lang = null
  switch (reaction._emoji.name) {
    case '🇵🇱': lang = 'pl'; break;
    case '🇺🇸': lang = 'en'; break;
    case '🇬🇧': lang = 'en'; break;
    case '🇭🇺': lang = 'hu'; break;
    case '🇩🇪': lang = 'de'; break;
    case '🇮🇹': lang = 'it'; break;
    case '🇨🇿': lang = 'cs'; break;
  }
  if (lang) {
    translate(String(reaction.message.content), {to: lang}).then(res => {
      reaction.message.reply( reaction.message.author.username + ' means: ' + res.text)
    }).catch(err => console.error(err))
  }
})

// this has to be at the end
client.login(botToken)
